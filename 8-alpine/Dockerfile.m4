#
# Use Jeeon's node base image
#

FROM jeeon/node:8-alpine

include(common/maintainer.dockerfile)

include(common/install-chromium-alpine.dockerfile)
