# docker-node-chrome

Ready-made docker image for building and end-to-end testing of
**NodeJS Apps** using **Headless Chrome**.

Very specific versions (some non-latest) are used.

- node (using base image jeeon/node)
    - @ 6-default
    - @ 6-alpine
    - @ 8-default
    - @ 8-alpine
    - @ 10-stretch *(latest)*
    - @ 10-alpine
- chromium 
    - @ 72 *(prefer latest)*

### Pull from Docker Hub
```
docker pull jeeon/node-chrome:6-default
docker pull jeeon/node-chrome:6-alpine
docker pull jeeon/node-chrome:8-default
docker pull jeeon/node-chrome:8-alpine
docker pull jeeon/node-chrome:10-default
docker pull jeeon/node-chrome:10-alpine
```

### Build from GitLab
```
docker build -t jeeon/node-chrome:6-default gitlab.com/jeeon/docker/node-chrome/6-default
docker build -t jeeon/node-chrome:6-alpine gitlab.com/jeeon/docker/node-chrome/6-alpine
docker build -t jeeon/node-chrome:8-default gitlab.com/jeeon/docker/node-chrome/8-default
docker build -t jeeon/node-chrome:8-alpine gitlab.com/jeeon/docker/node-chrome/8-alpine
docker build -t jeeon/node-chrome:10-default gitlab.com/jeeon/docker/node-chrome/10-default
docker build -t jeeon/node-chrome:10-alpine gitlab.com/jeeon/docker/node-chrome/10-alpine
```

### Run image
```
docker run -it jeeon/node-chrome:6-default bash
docker run -it jeeon/node-chrome:6-alpine sh
docker run -it jeeon/node-chrome:8-default bash
docker run -it jeeon/node-chrome:8-alpine sh
docker run -it jeeon/node-chrome:10-default bash
docker run -it jeeon/node-chrome:10-alpine sh
```

### Use as base image
```Dockerfile
FROM jeeon/node-chrome:6-default
FROM jeeon/node-chrome:6-alpine
FROM jeeon/node-chrome:8-default
FROM jeeon/node-chrome:8-alpine
FROM jeeon/node-chrome:10-default
FROM jeeon/node-chrome:10-alpine
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
