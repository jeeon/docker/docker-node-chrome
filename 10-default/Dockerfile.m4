#
# Use Jeeon's node base image
#

FROM jeeon/node:10-default

include(common/maintainer.dockerfile)

include(common/install-chromium-debian.dockerfile)
