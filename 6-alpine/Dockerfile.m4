#
# Use Jeeon's node base image
#

FROM jeeon/node:6-alpine

include(common/maintainer.dockerfile)

include(common/install-chromium-alpine.dockerfile)
