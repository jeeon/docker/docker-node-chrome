# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 2019-03-06

### New

- Auto-trigger building dependant docker images.

## 2019-03-01

### New

- Add docker images for Node 10 (based on stretch and alpine).

### Changed

- Make code more DRY by using `m4` to generate `Dockerfile`s from common snippets.
- Upgrade all system packages.
- Upgrade chromium to v72.0.3626.96-1~deb9u2 (in debian stretch) and v72.0.3626.109-r0 (in alpine).

## 2019-01-05

### Changed

- Fresh rebuild to get latest system updates and renew latest android sdk licences.
- Upgrade chromium to v71.0.3578.80-1~deb9u1 (in debian stretch) and v71.0.3578.98-r2 (in alpine).

## 2018-10-13

### Changed

- Upgrade chromium to v69.0.3497.92-1~deb9u1 (in debian stretch) and v68.0.3440.106-r0 (in alpine).
- Upgrade ca-certificates to v20161130+nmu1+deb9u1 (in debian stretch).

## 2018-05-11

### Changed

- Remove npm cache folder after installing packages.
- Upgrade yarn to v1.6.0 and npm to v6.0.1.
- Update chromium to v64.0.3282.168-r0 (in alpine) and v66.0.3359.117-1~deb9u1 (in debian stretch).
- Upgrade all system packages.
- Move upgrades of npm, yarn, gulp, and system packages to own docker image.

## 2018-04-04

### Changed

- Update docs about preferred dependency versions.

## 2018-04-01

### Changed

- Start keeping a ChangeLog.
- Upgrade chromium to v64.0.3282.119.
