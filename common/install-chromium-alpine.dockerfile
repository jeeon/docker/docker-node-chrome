#
# Install Chromium (latest)
#

ENV EDGE_MAIN_REPOSITORY=http://dl-cdn.alpinelinux.org/alpine/edge/main \
    EDGE_COMMUNITY_REPOSITORY=http://dl-cdn.alpinelinux.org/alpine/edge/community \
    \
    CHROMIUM_VERSION=72.0.3626.109-r0

RUN apk add --no-cache --upgrade \
        --repository $EDGE_MAIN_REPOSITORY \
        --repository $EDGE_COMMUNITY_REPOSITORY \
        musl \
        musl-utils && \
    apk add --no-cache \
        --repository $EDGE_MAIN_REPOSITORY \
        --repository $EDGE_COMMUNITY_REPOSITORY \
		chromium="$CHROMIUM_VERSION" && \
    \
    rm  -rf \
        /var/cache/apk/* \
        /var/tmp/* \
        /tmp/*
