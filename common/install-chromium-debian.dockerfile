#
# Install Chromium (latest)
#

ENV CHROMIUM_VERSION=72.0.3626.96-1~deb9u2 \
    CA_CERTIFICATES_VERSION=20161130+nmu1+deb9u1

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
        chromium="$CHROMIUM_VERSION" \
        ca-certificates="$CA_CERTIFICATES_VERSION" && \
    \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf \
        /etc/apt/sources.list.d/google-chrome.list \
        /var/lib/apt/lists/* \
        /var/cache/apt/* \
        /var/tmp/* \
        /tmp/*
