#
# Use Jeeon's node base image
#

FROM jeeon/node:10-alpine

include(common/maintainer.dockerfile)

include(common/install-chromium-alpine.dockerfile)
