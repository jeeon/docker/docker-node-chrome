#
# Use Jeeon's node base image
#

FROM jeeon/node:8-default

include(common/maintainer.dockerfile)

include(common/install-chromium-debian.dockerfile)
